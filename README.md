# Docker joal

This docker set up joal which fakes upload and download stats of a torrent to almost all bittorrent trackers.

Do not expose to the outside world as it is only 'secured' through obfuscation. We just use 'joal' as path prefix and 'joal' as secret token.
Please visit https://github.com/anthonyraymond/joal for more info.

#### Create and run home docker as detached
    docker-compose up -d

#### Create and run home docker
    docker-compose up

#### Stop home docker and related network
    docker-compose down

#### Access to logs and follow
    docker-compose logs -f

#### Pull image
    docker-compose pull

#### Open terminal on docker container
    docker containerid exec -it dockname /bin/bash

After start, visit SERVER_URL:PORT/joal/ui to set-up connection settings with WebUI
Add a few torrents with high leech and seed from WebUI or to config/torrents/
You can update config in config.json file or with WebUI.
